"""
@author: Ankit Dimri
AxEngine - Fans, Compressors & Turbine
A GUI Application for 1D, 2D, 3D Gas Turbine Design
"""
import sys, os
from PyQt5 import QtGui, QtWidgets

from AxEng.control.control import AxEngGUI
import os
#os.environ['ETS_TOOLKIT'] = 'qt5'
# By default, the PySide binding will be used. If you want the PyQt bindings
# to be used, you need to set the QT_API environment variable to 'pyqt'
#os.environ['QT_API'] = 'pyqt'

# To be able to use PySide or PyQt4 and not run in conflicts with traits,
# we need to import QtGui and QtCore from pyface.qt
from pyface.qt import QtGui
# Alternatively, you can bypass this line, but you need to make sure that
# the following lines are executed before the import of PyQT:
#   import sip
#   sip.setapi('QString', 2)

from traits.api import HasTraits, Instance, on_trait_change
from traitsui.api import View, Item
from mayavi.core.ui.api import MayaviScene, MlabSceneModel, \
        SceneEditor

from mayavi import mlab

__author__ = ['Ankit Dimri']

__copyright__ = "Copyright 2020, Ankit Dimri"

__credits__ = []

__developers__ = []

__license__ = ""

__version__ = "0.0.1"

class Visualization(HasTraits):
    scene = Instance(MlabSceneModel, ())
    @on_trait_change('scene.activated')
    def update_plot(self):
        # This function is called when the view is opened. We don't
        # populate the scene when the view is not yet open, as some
        # VTK features require a GLContext.

        # We can do normal mlab calls on the embedded scene.
        #self.scene.mlab.test_mesh_sphere()
        pass
        #mlab.test_mesh_sphere()


    # the layout of the dialog screated
    view = View(Item('scene', editor=SceneEditor(scene_class=MayaviScene),
                     height=250, width=300, show_label=False),
                resizable=True # We need this to resize with the parent widget
                )

################################################################################
# The QWidget containing the visualization, this is pure PyQt4 code.
class MayaviQWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        layout = QtGui.QVBoxLayout(self)
        layout.setContentsMargins(0,0,0,0)
        layout.setSpacing(0)
        self.visualization = Visualization()

        # If you want to debug, beware that you need to remove the Qt
        # input hook.
        #QtCore.pyqtRemoveInputHook()
        #import pdb ; pdb.set_trace()
        #QtCore.pyqtRestoreInputHook()

        # The edit_traits call will generate the widget to embed.
        self.ui = self.visualization.edit_traits(parent=self,
                                                 kind='subpanel', scrollable=False).control
        layout.addWidget(self.ui)
        self.ui.setParent(self)


def splash_screen():
    pass

def display_logo():
    pass

def parse_args(argv):
    pass

def run_batch():
    pass

def launch_docs():
    pass

def main(sys_args):
    if not QtWidgets.QApplication.instance():
        app = QtWidgets.QApplication(sys.argv)
    else:
        app = QtWidgets.QApplication.instance()

    mayavi_widget = MayaviQWidget()
    my_view = AxEngGUI(mayavi_widget)
    # define a "complex" layout to test the behaviour
    sys.exit(app.exec_())


if __name__ == '__main__':
    os.system('cls')
    main(sys.argv[1:])


