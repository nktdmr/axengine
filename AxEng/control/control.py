

from PyQt5 import QtGui, QtWidgets
import math
from AxEng.view.AxEngMain import Ui_AxEngGUI as Ax_EngGUI
from mayavi import mlab
import numpy as np
import pyqtgraph as pg
import matplotlib.pyplot as plt
from pyqtgraph.console import ConsoleWidget
from pyqtgraph import DataTreeWidget
from AxEng.model.geometry.airfoil.naca import NACA4Digit
from AxEng.model.geometry.airfoil.conformal import joukowsky_profile
from AxEng.model.solver.panelsover.panel import PanelMethod

class AxEngGUI(QtWidgets.QMainWindow):

    def __init__(self, MayaViwidget):
        """

        :param MayaViwidget(AxEng.__main__.MayaviQWidget) : AxEng.__main__.MayaviQWidget
        """
        super(AxEngGUI, self).__init__()

        self.ui = Ax_EngGUI()
        self.ui.setupUi(self)
        #self.ui.layoutCentral.addWidget(MayaViwidget)



        self.init_gui()
        self.setup_gui_events()

        self.setCentralWidget(MayaViwidget)
        self.showMaximized()

    def init_gui(self):
        #mlab.test_flow()
        #mlab.test_molecule()
        namespace = {'mlab': mlab, 'np': np, 'pg': pg, 'plt': plt, 'naca': NACA4Digit}
        console = ConsoleWidget(namespace=namespace)
        self.ui.layoutHOutput.addWidget(console)
        console2 = ConsoleWidget(namespace=namespace)
        console2.ui.historyBtn.hide()
        console2.ui.exceptionBtn.hide()

        objView = objectViewWidget()
        self.ui.layoutVObjectView.addWidget(objView)
        plotView = objectViewWidget()
        self.ui.layoutVPlotSettings.addWidget(plotView)

        self.ui.layouHtConsole.addWidget(console2)
        self.tabifyDockWidget(self.ui.dockInputView, self.ui.dockPlotSettings)
        self.tabifyDockWidget(self.ui.dockPlotSettings, self.ui.dockSystemView)
        self.tabifyDockWidget(self.ui.dockEngineConfig, self.ui.dockObjectView)
        self.tabifyDockWidget(self.ui.dockConsoleView, self.ui.dockOutputview)
        x = []
        y = []
        z = []
        zzz = []
        cp = []
        zz = [.1,.2, .3, .4, .5, .6,.7,.8,.9,1.0]
        zz.reverse()
        scalar = [10, 1, 20, 38, 38, 29, 89, 27, 28, 21, 123,12, 83, 27, 28, 78, 17, 81]
        angles = [1, 2, 3, 4, 5,6 ,7, 8, 9 , 10]
        chords = [1.0, 0.95, .9, 0.85, 0.80, 0.75, 0.70, 0.65, 0.6, 0.55, 0.5]
        chords.reverse()
        naca1 = [4404, 4406, 4407, 4408, 4409, 4410, 4411, 4412,4413, 4415]
        for z1, angle, naca, chord in zip(zz, angles, naca1, chords):
            a = NACA4Digit(str(naca+10), chord=chord)

            cp1 = PanelMethod(airfoil=a, aoa=angle).cpr[1]
            vel = np.sqrt(np.array((1 - cp1)*100.**2))
            ox, oy = np.array([.25]*len(a.cords[0])), np.array([0]*len(a.cords[0]))

            px, py = a.cords[0], a.cords[1]

            angle = .0174533* angle*5
            qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
            qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)

            x.append(np.array(qx))
            y.append(np.array(qy))
            z.append(np.array([z1*2.0]*len(qx)))
            cp.append(np.array(vel))

        x = np.array(x)
        y = np.array(y)
        z = np.array(z)
        cp = np.array(cp)








        '''for zz in z:
           x = x + ps[0]
           x= x +  ss[0]
           y.append(ps[1])
           y.append(ps[1])
           zzz[zz]*200)'''

        #mlab.mesh(x,y,[1]*200)
        #mlab.test_flow()

        a1 = mlab.mesh(x, y, z, scalars=cp, colormap='jet')


        #mlab.test_surf()

    def rotate(self, p, origin=(0, 0), degrees=0):
        angle = np.deg2rad(degrees)
        R = np.array([[np.cos(angle), -np.sin(angle)],
                      [np.sin(angle), np.cos(angle)]])
        o = np.atleast_2d(origin)
        p = np.atleast_2d(p)
        return np.squeeze((R @ (p.T - o.T) + o.T).T)

    def setup_gui_events(self):
        pass

    def update_property_view(self):
        pass

    def update_plot_view(self):
        pass

    def update_system_view(self):
        pass

class objectViewWidget(DataTreeWidget):
    pass

class MayaviView():
    pass

class PlotView():
    pass

class CompressorMapView():
    pass

class VelocityTriangleView():
    pass

class MeanlineView():
    pass

class ThroughFlowView():
    pass

class CFD3DView():
    pass

class AirfoilView():
    pass

class BladetoBladeView():
    pass

class S1S2View():
    pass

class BoundaryLayerView():
    pass




