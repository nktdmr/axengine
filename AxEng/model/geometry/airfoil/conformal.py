import numpy as np
from AxEng.model.geometry.airfoil.airfoil import Airfoil
import matplotlib.pyplot as plt
"""
Doc String for Class Kutta Jukowoski transformation/ Conformal transformation
"""


class joukowsky_profile(Airfoil):
    """
    Kutta joukowsky Airfoils
    """

    def __init__(self, dx, dy):
        """
        constructor for
        :param dx: Value of delta x shift from centre/origin
        :param dy: Value of delta y shift from centre/origin
        """
        super(joukowsky_profile, self).__init__()
        self.dx = dx
        self.dy = dy


    @property
    def profile_calc(self):
        radian = np.pi / 180.0
        nump = self.npt
        dx = self.dx
        dx = np.sqrt(dx ** 2)
        dy = self.dy
        radius = 1.0
        zeta = []
        neta = []
        b = radius - dx
        beta = np.arcsin(dy / radius)
        expr = dx / b
        c1 = 180.0 * radian
        c2 = np.cos(c1)
        c3 = np.sin(c1)
        c4 = (b + dx) * c2
        c5 = (b + dx) * c3
        c6 = c4 + dx
        c7 = c5 + b * beta * (1 + expr)
        c8 = c6 ** 2 + c7 ** 2
        c9 = b ** 2 / c8
        zeta1 = c6 * (1 + c9)
        neta1 = c7 * (1 - c9)
        c1 = 0 * radian
        c2 = np.cos(c1)
        c3 = np.sin(c1)
        c4 = (b + dx) * c2
        c5 = (b + dx) * c3
        c6 = c4 + dx
        c7 = c5 + b * beta * (1 + expr)
        c8 = c6 ** 2 + c7 ** 2
        c9 = b ** 2 / c8
        zeta2 = c6 * (1 + c9)
        neta2 = c7 * (1 - c9)
        chord = np.sqrt(pow((zeta2 - zeta1), 2) + pow((neta2 - neta1), 2))
        t_max = chord * expr * 100 * (1 + np.cos(60 * radian)) * np.sin(60 * radian) / chord
        print (beta)
        for i in range(0, nump + 1):
            c1 = (nump - i) * radian * (360.0 / nump)
            c2 = np.cos(c1)
            c3 = np.sin(c1)
            c4 = (b + dx) * c2
            c5 = (b + dx) * c3
            c6 = c4 + dx
            c7 = c5 + b * beta * (1 + expr)
            c8 = pow(c6, 2) + pow(c7, 2)
            c9 = pow(b, 2) / c8
            zeta.append(-((c6 * (1 + c9) - zeta2) / chord) * self.chord)
            neta.append(c7 * (1 - c9) / chord * self.chord)

        return zeta, neta, t_max


a = joukowsky_profile(0.105, 0.04)
zeta, neta, tmax = a.profile_calc
plt.plot(zeta, neta, '-x')

plt.gca().set_aspect('equal', adjustable='box')
print (tmax)
plt.show()


