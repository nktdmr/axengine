
from AxEng.model.geometry.airfoil.airfoil import Airfoil
import numpy as np

class NACA(Airfoil):
    """
    Generic NACA class which store all the coordinates
    LE->SS->TE->PS
    """

    def __init__(self, naca, chord=1.0):
        """
        Constructor for Generic NACA Airfoil
        :param naca: naca number
        """
        super(NACA, self).__init__('NACA-' + naca, chord=chord)
        self.naca = naca
        self.cords = self.coordinate


    @staticmethod
    def thickness(xc, t):
        """
        Return Thickness value Abs
        :param xc: x / chord ratio
        :param t: respective thickness at x/c
        :return: Thickness at x
        """
        a0 = 0.2969
        a1 = 0.126
        a2 = 0.3516
        a3 = 0.2843
        a4 = 0.1015
        yt = 5 * t * ((a0 * (xc ** 0.5)) - (a1 * xc) -
                      (a2 * (xc ** 2)) + (a3 * (xc ** 3)) - (a4 * (xc ** 4)))
        return yt

    @property
    def coordinate(self):
        """
        append all coordinate in clockwise direction
        :return:
        """
        ux = self.pressure_side()[0]
        lex = self.leading_edge()[0]
        x_cord_u = ux + lex
        uy = self.pressure_side()[1]
        ley = self.leading_edge()[1]
        y_cord_u = uy + ley
        lx = self.suction_side()[0]
        # tex = self.trailing_edge()[0]
        x_cord_l = lx
        x_cord_l.reverse()
        ly = self.suction_side()[1]
        # tey = self.trailing_edge()[1]
        y_cord_l = ly
        y_cord_l.reverse()
        x_coord = x_cord_u + x_cord_l
        y_coord = y_cord_u + y_cord_l

        return x_coord, y_coord

    def write_csv(self):
        """

        :return:
        """
        f = open('naca.csv', 'w')
        x = self.cords[0]
        y = self.cords[1]
        for i in range(0, len(self.cords[0])):
            s = str(x[i]) + ',  ' + str(y[i]) + ',  ' + str(0.0) + '\n'
            f.write(s)

class NACA4Digit(NACA):

    """
    Class for NACA 4 Digit Airfoils
    p-> position->first digit
    m-> camber value->second digit
    t-> thickness->third digit
    """

    def __init__(self, naca='4415', chord=1.0):
        """
        Constructor of NACA 4 Digit Airfoil
        :type naca: NACA number
        :rtype: none
        :param naca: NACA number which is passed to super class
        """
        super(NACA4Digit, self).__init__(naca, chord=chord)

    @property
    def npt(self):
        """
        Number of Points
        :return:
        """
        return 100

    @property
    def m(self):
        """
        Camber Value
        :return: Camber value 100th of the first digit
        """
        m = self.naca[0]
        m = float(m)
        m /= 100.0
        return m

    @property
    def p(self):
        """
        Position
        :return: Position value 10th of second digit
        """
        p = self.naca[1]
        p = float(p)
        p /= 10.0
        return p

    @property
    def t(self):
        """
        Thickness value relative
        :return:MAX Thickness of the airfoil at 30% of chord from LE. it is 100th of the last two digit of NACA Number
        """
        t = self.naca[-2:]
        t = float(t)
        t = t / 100.0 * self.chord
        return t

    def pressure_side(self):
        """
        Coordinates of Lower Side
        :return: Coordinates of Lower side i.e Pressure side
        """
        x_cord = []
        y_cord = []
        c = self.chord
        t = self.t
        i = 0
        while i < self.npt:
            x = self.x_pos(i)
            xc = x / c
            yt = self.thickness(xc, t)
            yc = self.y_camber(xc)[0]
            theta = self.y_camber(xc)[1]
            x_cord.append(x + yt * np.sin(theta))
            y_cord.append(yc - yt * np.cos(theta))
            i = i + 1
        lower_side = (x_cord, y_cord)
        return lower_side

    def suction_side(self):
        """
        Coordinates of Upper Side
        :return: returns Coordinates of Upper Side that is Suction side
        """
        x_cord = []
        y_cord = []
        c = self.chord
        npt = self.npt
        t = self.t
        i = 0
        while i < npt:
            x = self.x_pos(i)
            xc = x / c
            yt = self.thickness(xc, t)
            yc = self.y_camber(xc)[0]
            theta = self.y_camber(xc)[1]
            x_cord.append(x - yt * np.sin(theta))
            y_cord.append(yc + yt * np.cos(theta))
            i = i + 1
        upper_side = (x_cord, y_cord)
        return upper_side

    def x_pos(self, i):
        """
        Position of x coordinate
        :param i: index of airfoil coordinate
        :return: Return absolute x value of airfoil along the chord
        """
        c = self.chord
        npt = self.npt
        x = c * 0.5 * (np.cos(i * (np.pi / npt)) + 1)
        return x

    def y_camber(self, xc):
        """
        y value of camber line
        :param xc:x/chord ratio
        :return: return value of camber at x/c
        """
        p = self.p
        m = self.m
        c = self.chord

        if xc <= p:
            yc = m * c / p ** 2 * (2 * p * xc - xc ** 2)
            dyc_dx = 2 * m / (p ** 2) * (p - xc)
            theta = np.arctan(dyc_dx)
        else:
            yc = m * c / (1 - p) ** 2 * \
                 ((1 - (2 * p) + (2 * p * xc)) - xc ** 2)
            dyc_dx = 2 * m / (1 - p) ** 2 * (p - xc)
            theta = np.arctan(dyc_dx)
        return yc, theta