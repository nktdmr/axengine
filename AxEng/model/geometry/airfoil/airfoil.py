class Airfoil(object):
    """
    Class for Airfoil
    """
    def __init__(self, title='Airfoil', chord=1.0):
        self.title = title
        self._chord = chord

    def leading_edge(self):
        self.a = 10
        return [0.0], [0.0]

    def trailing_edge(self):
        return [self.chord], [0.0]

    def pressure_side(self):
        return [0.0], [0.0]

    def suction_side(self):
        return [0.0], [0.0]

    @property
    def npt(self):
        return 100

    @property
    def chord(self):
        return self._chord

    @chord.setter
    def chord(self, value):
        self._chord = value