import numpy as np
import matplotlib.pyplot as plt
from AxEng.model.geometry.airfoil.naca import NACA4Digit
import time


class PanelMethod(object):
    """
    This Hess and Smith Panel Method
    """

    def __init__(self, airfoil=NACA4Digit('4415'), aoa=0.0, vel=100.0, analysisMode=False):
        """
        Constructor of Panel Method- Hess and Smith Panel Method for Invicid , Incompressible and Steady Flow
        :param airfoil: Airfoil in which panel Method needs to be applied
        :param aoa: Angle of Attack at which the calculation needs to be done
        :param vel: Velocity of Freestream Air
        """
        self.analysisMode = analysisMode
        self.airfoil_cords = airfoil.cords
        self.title = airfoil.title
        self.chord = airfoil.chord
        self.x = self.airfoil_cords[0]
        self.y = self.airfoil_cords[1]
        self.npt = len(airfoil.cords[0])
        self.aoa = aoa
        self.vel = vel
        self.cpr = self.panel_method()
        self.cpAlpha = self.results(self.cpr[1], aoa)
        self.results = self.results(self.cpr[1], aoa)

    def panel_method(self):
        """
        MAin Method for calculation
        :return: Chords Vs coefficient of in_pressure List
        """
        n = self.npt
        x = self.x
        y = self.y
        a = np.ndarray(shape=(n, n), dtype=float, order='C')
        bi = np.ndarray(shape=n, dtype=float, order='C')
        xm = np.ndarray(shape=(n - 1), dtype=float, order='C')
        ym = np.ndarray(shape=(n - 1), dtype=float, order='C')
        r = np.ndarray(shape=(n - 1, n), dtype=float, order='C')
        l = np.ndarray(shape=(n - 1), dtype=float, order='C')
        theta = np.ndarray(shape=(n - 1), dtype=float, order='C')
        beta = np.ndarray(shape=(n - 1, n - 1), dtype=float, order='C')
        mid_point_x = np.ndarray(shape=(n - 1), dtype=float, order='C')
        mid_point_y = np.ndarray(shape=(n - 1), dtype=float, order='C')
        temp = 0.0
        temp2 = 0.0
        i = 0
        while i < n - 1:
            xm[i] = (x[i] + x[i + 1]) / 2.0
            ym[i] = (y[i] + y[i + 1]) / 2.0
            mid_point_x[i] = xm[i]
            mid_point_y[i] = ym[i]
            l[i] = np.sqrt((x[i + 1] - x[i]) ** 2 + (y[i + 1] - y[i]) ** 2)
            theta[i] = np.arctan2(y[i + 1] - y[i], x[i + 1] - x[i])
            i += 1

        for i in range(0, n - 1):
            for j in range(0, n):
                r[i, j] = np.sqrt((x[j] - xm[i]) ** 2 + (y[j] - ym[i]) ** 2)
                if j < n - 1:
                    beta[i, j] = np.arctan2((((ym[i] - y[j + 1]) * (xm[i] - x[j]))
                                             - ((xm[i] - x[j + 1]) * (ym[i] - y[j]))),
                                            ((xm[i] - x[j + 1]) * (xm[i] - x[j]))
                                            + ((ym[i] - y[j + 1]) * (ym[i] - y[j])))
            beta[i, i] = np.pi
        aoa = (self.aoa * np.pi) / 180.0
        if not self.analysisMode:
            pass
            #ppb(0, n - 2, prefix='Progress:', suffix='Complete', length=75)
        for i in range(0, n - 1):
            temp2 = np.sin(theta[0] - theta[i]) * np.log(r[0, i + 1] / r[0, i]) + \
                    np.sin(theta[n - 2] - theta[i]) * np.log(r[n - 2, i + 1] / r[n - 2, i]) + \
                    np.cos(theta[0] - theta[i]) * beta[0, i] + np.cos(theta[n - 2] - theta[i]) * beta[n - 2, i] + temp2
            for j in range(0, n - 1):
                a[i, j] = (np.sin(theta[i] - theta[j]) * np.log(r[i, j + 1] / r[i, j]) +
                           np.cos(theta[i] - theta[j]) * beta[i, j])
                a[i, j] /= 2 * np.pi
                a[i, n - 1] = (np.cos(theta[i] - theta[j]) * np.log(r[i, j + 1] / r[i, j])) - (
                    np.sin(theta[i] - theta[j]) * beta[i, j]) + temp
                temp = a[i, n - 1]
                a[n - 1, j] = ((np.sin(theta[0] - theta[j]) * beta[0, j]) +
                               (np.sin(theta[n - 2] - theta[j]) * beta[n - 2, j]) -
                               (np.cos(theta[0] - theta[j]) * np.log(r[0, j + 1] / r[0, j])) -
                               (np.cos(theta[n - 2] - theta[j]) * np.log(r[n - 2, j + 1] / r[n - 2, j])))
                a[n - 1, j] /= 2 * np.pi
            if not self.analysisMode:
                pass
                #(i, n - 2, prefix='Progress:', suffix='Complete')

            bi[i] = self.vel * np.sin(theta[i] - aoa)
            a[i, n - 1] = temp / (2 * np.pi)
            temp = 0
        a[n - 1, n - 1] = temp2 / (2 * np.pi)
        bi[n - 1] = -self.vel * np.cos(theta[0] - aoa) - self.vel * np.cos(theta[n - 2] - aoa)
        q = np.linalg.solve(a, bi)
        velocity = self.velocity(theta, q, r, beta, aoa, self.vel)
        cp = self.cp(velocity)
        return self.x_pos(), cp

    def x_pos(self):
        """
        Position of chord
        :return: List of x position based on number of points required
        """
        n = self.npt
        x = np.ndarray(shape=n, dtype=float, order='C')
        for i in range(0, n - 1):
            x[i] = self.chord * 0.5 * (np.cos(i * (2 * np.pi / n)) + 1)
        return x

    def velocity(self, theta, q, r, beta, aoa, vel):
        """
        Velocity calculation at each point
        :param theta: Theta is the angle w.r.t to x axis made by each panel[list]
        :param q: q is the solution of aq=b[list]
        :param r: distance between midpoint of each panel with respect to other
        :param beta: while solving the linear eqs beta[list] is B in AX=B
        :param aoa: Angle of attack in degrees
        :param vel: Velocity of freeStream
        :return:
        """
        n = self.npt
        temp = 0.0
        temp3 = 0.0
        velocity = np.ndarray(shape=n, dtype=float, order='C')

        for i in range(0, n - 1):
            for j in range(0, n - 1):
                temp1 = (q[j] / (2 * np.pi)) * ((np.sin(theta[i] - theta[j]) * beta[i, j]) -
                                                (np.cos(theta[i] - theta[j]) * np.log(r[i, j + 1] / r[i, j]))) + temp
                temp = temp1
                temp2 = ((np.sin(theta[i] - theta[j]) * np.log(r[i, j + 1] / r[i, j])) +
                         (np.cos(theta[i] - theta[j]) * beta[i, j])) + temp3
                temp3 = temp2
            velocity[i] = (np.cos(theta[i] - aoa) * vel) + temp + ((temp3 / (2 * np.pi)) * (q[n - 1]))
            temp = 0.0
            temp3 = 0.0
        return velocity

    def cp(self, velocity):
        """

        :param velocity:
        :return:
        """
        n = self.npt
        cp = np.ndarray(shape=n, dtype=float, order='C')
        vel = self.vel
        for i in range(0, n - 1):
            cp[i] = 1 - ((velocity[i] ** 2) / (vel ** 2))
        return cp

    def results(self, cp, aoa):
        """

        :param cp:
        :param aoa:
        :return:
        """
        x = self.x
        y = self.y
        n = self.npt
        c = self.chord
        fx = np.ndarray(shape=(n-1), dtype=float, order='C')
        fy = np.ndarray(shape=(n-1), dtype=float, order='C')
        m = np.ndarray(shape=(n-1), dtype=float, order='C')

        total_fx = 0.0
        total_fy = 0.0
        total_cm = 0.0

        cp_min = cp[0]
        cp_max = cp[0]
        x_min = 0.0
        x_max = 0.0

        for i in range(0, n - 1):
            xc = self.chord * 0.5 * (np.cos(i * (2 * np.pi / n)) + 1)
            if cp_max < cp[i]:
                cp_max = cp[i]
            elif cp_min > cp[i]:
                cp_min = cp[i]
            if x_max < xc:
                x_max = xc
            elif x_min > xc:
                x_min = xc
            if cp_max < cp[n - 2 - i]:
                cp_max = cp[n - 2 - i]
            if cp_min > cp[n - 2 - i]:
                cp_min = cp[n - 2 - i]

            fx[i] = cp[i] * (y[i + 1] - y[i]) / c
            fy[i] = cp[i] * (x[i + 1] - x[i]) / c
            m[i] = (-fx[i] * ((y[i + 1] + y[i]) / 2 * c)) + (fy[i] * (((x[i + 1] + x[i]) / 2 * c) - (1 / 4)))

            total_fx = total_fx + fx[i]
            total_fy = total_fy + fy[i]
            total_cm = total_cm + m[i]
        total_fx = total_fx
        total_fy = total_fy
        aoa *= 2 * np.pi / 360
        total_cl = (np.cos(aoa) * total_fy) - (np.sin(aoa) * total_fx)
        total_cd = (np.cos(aoa) * total_fx) + (np.sin(aoa) * total_fy)

        return -total_cl, -total_cd, total_cm, cp_min
    '''
    def print_results(self):
        """

        :return:
        """
        print
        print_header('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
        pr_yellow('+                                                                ')
        print_header('+    Input:                                                   ')
        pr_green('+       Flow                 : Inviscid, Incompressible, Steady   ')
        pr_green('+       AOA                  : %s Degree               ' % self.aoa)
        pr_green('+       Velocity             : %s m/sec                ' % self.vel)
        pr_green('+       Airfoil              : %s                    ' % self.title)
        print_header('+    Results:                                                 ')
        pr_blue('+       Lift Coefficient     : %s           ' % str(self.results[0]))
        pr_blue('+       Drag Coefficient     : %s           ' % str(self.results[1]))
        pr_blue('+       Moment Coefficient   : %s           ' % str(self.results[2]))
        pr_blue('+       Minimum Cp           : %s           ' % str(self.results[3]))
        pr_blue('+                                                                  ')
        print_header('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
        print
        print
        return
    '''
    def __str__(self):
        self.print_results()
        return super(PanelMethod, self).__str__()


class RunAnalysis(object):
    """
    Run Analysis for calculating:
     clvsAplha curve
     cdvsAlpha curve
     cmvsAlpha curve
    """

    def __init__(self, start_aoa, end_aoa, step, pr=False):
        """
        :param start_aoa: Starting Angle of Attack
        :param end_aoa: End Angle of Attack
        :param step: Step
        :param pr: Print Results (True or False)
        """
        self.s_aoa = start_aoa
        self.e_aoa = end_aoa
        self.step = step
        self.prRes = pr

    def analysis_results(self):
        """

        :return: Lift Coefficients,
                 Drag Coefficients,
                 Coefficients of moment,
                 Minimum Cp,
                 Angle of Attacks
        """

        cl = []
        cd = []
        cm = []
        cp_min = []
        aoa = []
        l = (self.e_aoa - self.s_aoa) / self.step
        itr = 0.0
        print('\n')
        #print_magenta_high("Running Analysis Mode")
        print('\n')
        #ppb(0, l, prefix='Progress:', suffix='Complete')
        i = self.s_aoa
        start = time.clock()
        # your code here
        from mpl_toolkits.mplot3d import Axes3D
        fig = plt.figure()
        fig.add_subplot()
        while i <= self.e_aoa:
            pnl_analysis = PanelMethod(aoa=i, analysisMode=True)

            plt.plot(pnl_analysis.cpr[0][:-1], pnl_analysis.cpr[1][:-1], '-o',
                     scaley=True, scalex=True, label='Angle of Attack %s' % i)
            plt.legend(loc='lower right')
            if self.prRes:
                pass
                #pnl_analysis.print_results()
            results = pnl_analysis.results
            cl.append(results[0])
            cd.append(results[1])
            cm.append(results[2])
            cp_min.append(results[3])
            aoa.append(i)
            #ppb(itr, l, prefix='Progress:', suffix='Complete')
            i += self.step
            itr += 1
        print('Time Taken:  ' + str(time.clock() - start) + ' sec')
        plt.gca().set_aspect('auto')
        plt.show()
        return [cl, cd, cm, cp_min, aoa]